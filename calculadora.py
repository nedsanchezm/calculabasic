1#Calculadora Basica En Python

class Calculadora:
    #metodo constructor de la clase
    def _init_(self):
        self.operado_1=0
        self.operando_2=0
        self.resultado=0
    

    def sumar (self) :
        self.resultado = self.operado_1 +  self.operando_2  

    def restar (self) :
        self.resultado = self.operado_1 - self.operando_2 

    def dividir (self) :
        #TO DO: EVITAR DIVISION CERO.
        self.resultado = self.operado_1 / self.operando_2

    def multiplicar (self) :
       self.resultado = self.operado_1 * self.operando_2
       
    def modulo (self) :
       if self.operando_2 == 0:
          self.resultado = 0
       else:
           self.resultado = self.operado_1 % self.operando_2